package cc.luaq.tutorial.gui;

import cc.luaq.animate.animation.ExponentialFlux;
import cc.luaq.animate.animation.LinearFlux;
import cc.luaq.animate.animation.easing.EaseMode;
import cc.luaq.animate.animation.interfaces.IRenewableFlux;
import cc.luaq.animate.exceptions.AniMateException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

import java.awt.*;

/**
 * Copyright (c) 2019 luaq.dev
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class ExampleGUI extends GuiScreen {

    private IRenewableFlux flux;
    private IRenewableFlux leftFlux;

    public ExampleGUI() {
        this.mc = Minecraft.getMinecraft();
        this.flux = new LinearFlux(3000, 0F, 100F);
        this.leftFlux = new ExponentialFlux(3000, 0F, 600F);
        try {
            leftFlux.applyEaseMode(EaseMode.EASE_IN_OUT);
        } catch (AniMateException e) {}

        // this.leftFlux = new ExponentialFlux(3000, 0F, 600F, EaseMode.EASE_IN_OUT);
        // does the same thing                                 ^^^^^^^^^^^^^^^^^^^^
    }

    @Override
    public void initGui() {
        flux.startAnimation();
        leftFlux.startAnimation();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);

        drawRect(0, 0, this.width, this.height, new Color(0, 0, 0, (int) flux.calculateValue()).getRGB());

        String text = "Hello §cYouTube§r!";
        FontRenderer fontRenderer = mc.fontRendererObj;
        drawCenteredString(fontRenderer, text, (int) (this.width / 2 + leftFlux.calculateValue()) - 600, this.height / 2, new Color(255, 255, 255, 255).getRGB());
    }

    public void show() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void tick(TickEvent.ClientTickEvent event) {
        MinecraftForge.EVENT_BUS.unregister(this);
        this.mc.displayGuiScreen(this);
    }

}
